var ContactUs = function () {

    return {
        //main function to initiate the module
        init: function () {
			var map;
			$(document).ready(function(){
			  map = new GMaps({
				div: '#map',
	            lat: 10.3091858,
				lng: 123.9169523,
			  });
			   var marker = map.addMarker({
		            lat: 10.3091858,
					lng: 123.9169523,
		            title: 'LRA Customs Brokerage',
		            infoWindow: {
		                content: "<b>LRA Customs Brokerage</b><br>Unit 2D-2E 2nd Floor Sail Centre Commercial Complex<br>J.L. Briones Street, North Reclamation Area,<br>Cebu City, Philippines"
		            }
		        });

			   marker.infoWindow.open(map, marker);
			});
        }
    };

}();