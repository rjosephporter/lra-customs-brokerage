// Class to represent a new item
function Item(data, itemTotals, rates) {

	console.log(rates.forexRate());

	var self = this;

	self.itemName				=	ko.observable(data.itemName);
	self.tariffHeading			=	ko.observable(data.tariffHeading);
	self.rateOfCustomsDuty		=	ko.observable(data.rateOfCustomsDuty);
	self.itemWeight				=	ko.observable(data.itemWeight);
	self.fobValue				=	ko.observable(data.fobValue);
	
	self.insurance = ko.computed(function() {
		return Number(self.fobValue() * 0.04);
	});
	
	self.marineInsurance		=	ko.observable(data.marineInsurance);
	
	self.freight = ko.computed(function() {
		return Number(self.fobValue()) / itemTotals.fobValue() * Number(itemTotals.freight());
	});
	
	self.otherDutiableValue		=	ko.observable(data.otherDutiableValue);
	
	self.dutiableValueUsd = ko.computed(function() {
		return 	Number(self.fobValue()) +
				Number(self.insurance()) +
				Number(self.marineInsurance()) +
				Number(self.freight()) +
				Number(self.otherDutiableValue());
	});
	
	self.dutiableValuePhp = ko.computed(function() {
		return Number(self.dutiableValueUsd()) * Number(rates.forexRate());
	});
	
	self.customsDuty = ko.computed(function() {
		return Number(self.dutiableValuePhp()) * (Number(self.rateOfCustomsDuty()) / 100);
	});
	
	self.exciseTax				=	ko.observable(data.exciseTax);
	self.safeguardDuty			=	ko.observable(data.safeguardDuty);
	
	self.brokerageFee = ko.computed(function() {
		return Number(self.dutiableValuePhp()) / itemTotals.dutiableValuePhp() * Number(itemTotals.brokerageFee());
	});
	
	self.arrastreFee = ko.computed(function() {
		return Number(self.dutiableValuePhp()) / itemTotals.dutiableValuePhp() * Number(itemTotals.arrastreFee());
	});
	
	self.wharfageFee = ko.computed(function() {
		return Number(self.dutiableValuePhp()) / itemTotals.dutiableValuePhp() * Number(itemTotals.wharfageFee());
	});
	
	self.importProcessingFee = ko.computed(function() {
		return Number(self.dutiableValuePhp()) / itemTotals.dutiableValuePhp() * Number(itemTotals.importProcessingFee());
	});
	
	self.customsDocStamps = ko.computed(function() {
		return Number(self.dutiableValuePhp()) / itemTotals.dutiableValuePhp() * Number(itemTotals.customsDocStamps());
	});
	
	self.totalLandedCost = ko.computed(function() {
		return 	Number(self.dutiableValuePhp()) +
				Number(self.customsDuty()) +
				Number(self.exciseTax()) +
				Number(self.safeguardDuty()) +
				Number(self.brokerageFee()) + 
				Number(self.arrastreFee()) +
				Number(self.wharfageFee()) + 
				Number(self.importProcessingFee()) +
				Number(self.customsDocStamps());
	});
	
	self.valueAddedTax = ko.computed(function() {
		return self.totalLandedCost() * (Number(rates.taxRate()) / 100);
	});
}


// Overall viewmodel for this screen
function ShippingCalculatorViewModel(numOfItems) {
	
	// Variables
	var self = this;

	// Functions
	self.sumItem = function(particular) {
		var total = 0; 
		$.each(self.items(), function(ndx,val) { 
			total += Number(val[particular]()); 
		}); 
		return total;
	};

	// Non-editable data
	self.defaultItemData = {
		itemName: '',
		tariffHeading : null,
		rateOfCustomsDuty : 0,
		itemWeight : 0,
		fobValue : 0,
		insurance : 0,
		marineInsurance : 0,
		freight : 0,
		otherDutiableValue : 0,
		dutiableValueUsd : 0,
		dutiableValuePhp : 0,
		customsDuty : 0,
		exciseTax : 0,
		safeguardDuty : 0,
		brokerageFee : 0,
		arrastreFee : 0,
		wharfageFee : 0,
		importProcessingFee : 0,
		customsDocStamps : 0,
		totalLandedCost : 0,
		valueAddedTax : 0
	};

	// Editable data
	self.items = ko.observableArray();
	self.rates = {
		forexRate 	: ko.observable(),
		taxRate 	: ko.observable(12)
	};

	// Computed data
	self.itemTotals = {
		rateOfCustomsDuty 		: ko.computed(function() { return self.sumItem('rateOfCustomsDuty') }),
		itemWeight 				: ko.computed(function() { return self.sumItem('itemWeight') }),
		fobValue 				: ko.computed(function() { return self.sumItem('fobValue') }),
		insurance 				: ko.computed(function() { return self.sumItem('insurance') }),
		marineInsurance 		: ko.computed(function() { return self.sumItem('marineInsurance') }),
		freight 				: ko.observable(0),
		otherDutiableValue 		: ko.computed(function() { return self.sumItem('otherDutiableValue') }),
		dutiableValueUsd 		: ko.computed(function() { return self.sumItem('dutiableValueUsd') }),
		dutiableValuePhp 		: ko.computed(function() { return self.sumItem('dutiableValuePhp') }),
		customsDuty 			: ko.computed(function() { return self.sumItem('customsDuty') }),
		exciseTax 				: ko.computed(function() { return self.sumItem('exciseTax') }),
		safeguardDuty 			: ko.computed(function() { return self.sumItem('safeguardDuty') }),


		importProcessingFee 	: ko.observable(500),
		customsDocStamps 		: ko.observable(265),
		totalLandedCost 		: ko.computed(function() { return self.sumItem('totalLandedCost') }),
		valueAddedTax 			: ko.computed(function() { return self.sumItem('valueAddedTax') })		
	};
	
	self.itemTotals.brokerageFee = ko.computed(function() { 
		
		var dutiableValuePhp = self.itemTotals.dutiableValuePhp();
		var brokerageFee = 0;
		
		if(dutiableValuePhp >= 0 && dutiableValuePhp <= 10000)
			brokerageFee = 1300;
		else if(dutiableValuePhp > 10000 && dutiableValuePhp <= 20000)
			brokerageFee = 2000;
		else if(dutiableValuePhp > 20000 && dutiableValuePhp <= 30000)
			brokerageFee = 2700;
		else if(dutiableValuePhp > 30000 && dutiableValuePhp <= 40000)
			brokerageFee = 3300;
		else if(dutiableValuePhp > 40000 && dutiableValuePhp <= 50000)
			brokerageFee = 3600;
		else if(dutiableValuePhp > 50000 && dutiableValuePhp <= 60000)
			brokerageFee = 4000;
		else if(dutiableValuePhp > 60000 && dutiableValuePhp <= 100000)
			brokerageFee = 4700;
		else if(dutiableValuePhp > 100000 && dutiableValuePhp <= 200000)
			brokerageFee = 5300;
		else 
			brokerageFee = ((dutiableValuePhp - 200000) * 0.00125) + 5300; 
		
		return brokerageFee;
	});

	self.itemTotals.arrastreFee = ko.computed(function(){
		return self.itemTotals.itemWeight() / 1000 * 110;
	});

	self.itemTotals.wharfageFee = ko.computed(function() {
		return self.itemTotals.itemWeight() / 1000 * 34;
	});

	self.summary = {
		dutiesAndTaxes					: ko.computed(function() {
											var total = Number(self.itemTotals.customsDuty()) + 
														Number(self.itemTotals.valueAddedTax()) + 
														Number(self.itemTotals.exciseTax()) + 
														Number(self.itemTotals.safeguardDuty()) + 
														Number(self.itemTotals.importProcessingFee());
											return total.toFixed(2);
										}),
		brokerageFee 					: ko.computed(function() { return Number(self.itemTotals.brokerageFee()).toFixed(2) } ),
		terminalHandlingCharge			: ko.observable(0),
		containerDeposit				: ko.observable(0),
		encodingFee						: ko.observable(65),
		customsDocStamps				: ko.computed(function() { return Number(self.itemTotals.customsDocStamps()).toFixed(2) } ),
		sdvStamps						: ko.observable(115),

		demurrageCharges				: ko.observable(0),
		equipmentInsuranceClearance		: ko.observable(0),
		pdeaClearance					: ko.observable(0),
		customsFormsXeroxNotarial		: ko.observable(500),
		documentationProcessing			: ko.observable(1500),

		delivery						: ko.observable(0)
	};

	self.computation = {

		twentyFooter	: {
				quantity 	: ko.observable(0),
				arrastre 	: {
						rate 	: ko.observable(1587.71),						
				},
				wharfage 	: {
						rate 	: ko.observable(979.33),
				},
				storage 	: {
						rate 	: ko.observable(269.53),
						days 	: ko.observable(1),
				}
		},
		fortyFooter		: {
				quantity 	: ko.observable(0),
				arrastre 	: {
						rate 	: ko.observable(2654.85),						
				},
				wharfage 	: {
						rate 	: ko.observable(1537.31),
				},
				storage 	: {
						rate 	: ko.observable(539.06),
						days 	: ko.observable(1),
				}							
		},
		total 			: {}
	};

	// Arrastre
	self.computation.twentyFooter.arrastre.amount = ko.computed(function() {
		var twentyFooter = self.computation.twentyFooter;
		var amount = Number(twentyFooter.quantity()) * Number(twentyFooter.arrastre.rate());
		return amount.toFixed(2);
	});

	self.computation.fortyFooter.arrastre.amount = ko.computed(function() {
		var fortyFooter = self.computation.fortyFooter;
		var amount = Number(fortyFooter.quantity()) * Number(fortyFooter.arrastre.rate());
		return amount.toFixed(2);
	});

	self.computation.total.arrastre = ko.computed(function() {
		var computation = self.computation;
		var total = Number(computation.twentyFooter.arrastre.amount()) + Number(computation.fortyFooter.arrastre.amount());
		return total.toFixed(2);
	});

	// Wharfage
	self.computation.twentyFooter.wharfage.amount = ko.computed(function() {
		var twentyFooter = self.computation.twentyFooter;
		var amount = Number(twentyFooter.quantity()) * Number(twentyFooter.wharfage.rate());
		return amount.toFixed(2);
	});

	self.computation.fortyFooter.wharfage.amount = ko.computed(function() {
		var fortyFooter = self.computation.fortyFooter;
		var amount = Number(fortyFooter.quantity()) * Number(fortyFooter.wharfage.rate());
		return amount.toFixed(2);
	});

	self.computation.total.wharfage = ko.computed(function() {
		var computation = self.computation;
		var total = Number(computation.twentyFooter.wharfage.amount()) + Number(computation.fortyFooter.wharfage.amount());
		return total.toFixed(2);
	});

	// Storage	
	self.computation.twentyFooter.storage.amount = ko.computed(function() {
		var twentyFooter = self.computation.twentyFooter;
		var amount = (Number(twentyFooter.quantity()) * Number(twentyFooter.storage.rate())) * Number(twentyFooter.storage.days());
		return amount.toFixed(2);
	});

	self.computation.fortyFooter.storage.amount = ko.computed(function() {
		var fortyFooter = self.computation.fortyFooter;
		var amount = (Number(fortyFooter.quantity()) * Number(fortyFooter.storage.rate())) * Number(fortyFooter.storage.days());
		return amount.toFixed(2);
	});

	self.computation.total.storage = ko.computed(function() {
		var total = Number(self.computation.twentyFooter.storage.amount()) + Number(self.computation.fortyFooter.storage.amount());
		return total.toFixed(2);
	});	

	// Summary cont.
	self.summary.arrastre = ko.computed(function() { return self.computation.total.arrastre(); });
	self.summary.wharfage = ko.computed(function() { return self.computation.total.wharfage(); });
	self.summary.storageFee  = ko.computed(function() { return self.computation.total.storage(); });

	self.summary.vatOnBrokerageFee = ko.computed(function() {
		var vat = Number(self.summary.brokerageFee()) * 0.12;
		return vat.toFixed(2);
	});

	self.summary.portHandling = ko.computed(function() {
		var totalQty = Number(self.computation.twentyFooter.quantity()) + Number(self.computation.fortyFooter.quantity());
		var result = totalQty * 450;
		return result.toFixed(2);
	});

	self.summary.examination = ko.computed(function() {
		var totalQty = Number(self.computation.twentyFooter.quantity()) + Number(self.computation.fortyFooter.quantity());
		var result = totalQty * 500;
		return result.toFixed(2);
	});	

	self.summary.totalEstimatedCost = ko.computed(function() {
		var total = Number(self.summary.dutiesAndTaxes()) + 
					Number(self.summary.brokerageFee()) + 
					Number(self.summary.vatOnBrokerageFee()) + 
					Number(self.summary.terminalHandlingCharge()) + 
					Number(self.summary.containerDeposit()) + 
					Number(self.summary.encodingFee()) + 
					Number(self.summary.customsDocStamps()) + 
					Number(self.summary.sdvStamps()) + 
					Number(self.summary.arrastre()) + 
					Number(self.summary.wharfage()) + 
					Number(self.summary.storageFee()) + 
					Number(self.summary.demurrageCharges()) + 
					Number(self.summary.equipmentInsuranceClearance()) + 
					Number(self.summary.pdeaClearance()) + 
					Number(self.summary.customsFormsXeroxNotarial()) + 
					Number(self.summary.documentationProcessing()) + 
					Number(self.summary.portHandling()) + 
					Number(self.summary.examination()) + 
					Number(self.summary.delivery());
		return total.toFixed(2);
	});	

	// Operations
	self.addItem = function() {
		if(self.items().length > 9 )
			alert('You can only have a maximum of 10 items.')
		else
			self.items.push(new Item(self.defaultItemData, self.itemTotals, self.rates));

	};
	self.removeItem = function(item) {
		if(self.items().length == 1)
			alert('You must have at least 1 item.');
		else	
			self.items.remove(item);
	};

	self.submit = function() {
		console.log(ko.toJS(self.items));
	};

	self.printPreview = function() {
		console.log(ko.toJS(self.summary));
		console.log(ko.toJS(self.computation));

		var result = {
			summary : self.summary,
			computation : self.computation
		};

		window.open('duties-taxes-print?' + $.param(result));
	}

	$.ajax({
	  url: "http://www.freecurrencyconverterapi.com/api/v3/convert?q=USD_PHP&compact=y",
	  dataType: 'jsonp',
	  success: function(data){
	    self.rates.forexRate(data.USD_PHP.val);

		// Initialize number of items
		for(var i = 0; i < numOfItems; i++)
			self.items.push(new Item(self.defaultItemData, self.itemTotals, self.rates));		    
	  }
	});	
}

ko.applyBindings(new ShippingCalculatorViewModel(5));